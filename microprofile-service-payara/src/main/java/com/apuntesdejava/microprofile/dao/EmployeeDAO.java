/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apuntesdejava.microprofile.dao;

import com.apuntesdejava.microprofile.model.Employee;
import com.apuntesdejava.microprofile.request.EmployeeRequest;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@ApplicationScoped
public class EmployeeDAO {

    @Inject
    private EntityManager em;

    public List<Employee> findRange(int start, int count) {
        TypedQuery<Employee> query = em.createQuery("select e from Employee e", Employee.class)
                .setMaxResults(count)
                .setFirstResult(start);
        return query.getResultList();

    }

    @Transactional
    public Employee create(EmployeeRequest request) {
        Employee emp = new Employee();
        emp.setFirstName(request.getFirstName());
        emp.setLastName(request.getLastName());
        emp.setGender(request.getGender());
        emp.setBirthdate(request.getBirthdate());
        emp.setHireDate(request.getHireDate());
        em.persist(emp);
        return emp;
    }
}
