/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apuntesdejava.microprofile.service;

import com.apuntesdejava.microprofile.dao.EmployeeDAO;
import com.apuntesdejava.microprofile.model.Employee;
import com.apuntesdejava.microprofile.request.EmployeeRequest;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@ApplicationScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("employee")
public class EmployeeREST {

    @Inject
    private EmployeeDAO employeeDAO;

    @GET
    public Response findRange(@QueryParam("start") int start, @QueryParam("count") int count) {
        List<Employee> list = employeeDAO.findRange(start, count);
        return Response.ok(list).build();
    }

    @POST
    public Response create(EmployeeRequest request) {
        Employee emp = employeeDAO.create(request);
        return Response.ok(emp).build();
    }
}
