package com.apuntesdejava.microprofile.request;

import java.time.LocalDate;
import javax.json.bind.annotation.JsonbDateFormat;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
public class EmployeeRequest {

    private String firstName;
    private String lastName;
    @JsonbDateFormat(value = "yyyy-MM-dd")
    private LocalDate birthdate;
    private String gender;
    @JsonbDateFormat(value = "yyyy-MM-dd")
    private LocalDate hireDate;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDate hireDate) {
        this.hireDate = hireDate;
    }

}
